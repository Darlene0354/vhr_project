package org.javagirl.vhr_project.controller.salary;

import org.javagirl.vhr_project.controller.emp.EmployeeService;
import org.javagirl.vhr_project.model.Employee;
import org.javagirl.vhr_project.model.RespBean;
import org.javagirl.vhr_project.model.RespPageBean;
import org.javagirl.vhr_project.model.Salary;
import org.javagirl.vhr_project.service.SalaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/salary/sobcfg")
public class SobConfigController {
    @Autowired
    EmployeeService employeeService;
    @Autowired
    SalaryService salaryService;
    @GetMapping("/")
    public RespPageBean getEmployeeByPageWithSalary(@RequestParam(defaultValue = "1" ) Integer page,@RequestParam(defaultValue = "10") Integer size){
        return employeeService.getEmployeeByPageWithSalary(page,size);
    }

    @GetMapping("/salaries")
    public List<Salary> getAllSalaries(){
        return salaryService.getAllSalaries();
    }

    @PutMapping("/")
    public RespBean updateEmployeeSalaryById(Integer eid, Integer sid){
        int result=employeeService.updateEmployeeSalaryById(eid,sid);
        if(result == 1||result == 2){
            return RespBean.ok("更新成功");
        }
        return RespBean.error("更新失败");
    }
}
