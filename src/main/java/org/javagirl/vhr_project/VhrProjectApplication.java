package org.javagirl.vhr_project;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackages = "org.javagirl.vhr_project.mapper")
public class VhrProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(VhrProjectApplication.class, args);
    }

}
