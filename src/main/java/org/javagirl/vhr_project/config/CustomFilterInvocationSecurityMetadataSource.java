package org.javagirl.vhr_project.config;
import org.javagirl.vhr_project.model.Menu;
import org.javagirl.vhr_project.model.Role;
import org.javagirl.vhr_project.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;

import java.util.Collection;
import java.util.List;

/**
 * 这个类的作用，主要是根据用户传来的请求地址，分析出请求需要的角色
 * 获取该地址需要的用户角色
 */
@Component
public class CustomFilterInvocationSecurityMetadataSource implements FilterInvocationSecurityMetadataSource{

    @Autowired
    MenuService menuService;

    AntPathMatcher antPathMatcher = new AntPathMatcher();

//    请求匹配，没匹配上的登录之后访问
    @Override
    public Collection<ConfigAttribute> getAttributes(Object object) throws IllegalArgumentException {
        //获取请求地址
        String requestUrl=((FilterInvocation) object).getRequestUrl();
        //查询数据库中url pattern和role的对应关系
        List<Menu> menus=menuService.getAllMenusWithRole();
        for (Menu menu : menus) {
            //提取当前的请求url，将请求和数据库查询出来的所有路径匹配规则进行匹配
            if(antPathMatcher.match(menu.getUrl(),requestUrl)){
                //获取该路径对应的角色
                List<Role> roles = menu.getRoles();
                String[] str=new String[roles.size()];
                for (int i = 0; i < roles.size(); i++) {
                    str[i] = roles.get(i).getName();
                }
                return SecurityConfig.createList(str);

            }
        }
        //ROLE_LOGIN标记，没匹配上的，都是登陆访问
        return SecurityConfig.createList("ROLE_LOGIN");
    }

    @Override
    public Collection<ConfigAttribute> getAllConfigAttributes() {
        return null;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return false;
    }
}
