package org.javagirl.vhr_project.mapper;

import org.apache.ibatis.annotations.Param;
import org.javagirl.vhr_project.model.Salary;

import java.util.List;

public interface SalaryMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Salary record);

    int insertSelective(Salary record);

    Salary selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Salary record);

    int updateByPrimaryKey(Salary record);

    List<Salary> getAllSalaries();

    Integer deleteSalaryByIds(@Param("ids") Integer[] ids);
}