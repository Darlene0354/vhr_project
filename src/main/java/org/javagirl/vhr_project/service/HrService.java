package org.javagirl.vhr_project.service;

import org.javagirl.vhr_project.mapper.HrMapper;
import org.javagirl.vhr_project.mapper.HrRoleMapper;
import org.javagirl.vhr_project.model.Hr;
import org.javagirl.vhr_project.utils.HrUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * HrService实现UserDetailsService接口
 * 1.根据用户名查找用户
 * 2.查找、更新、删除hr
 * 3.更新hr角色
 */
@Service
public class HrService implements UserDetailsService {
    @Autowired
    HrMapper hrMapper;
    @Autowired
    HrRoleMapper hrRoleMapper;

    //在执行登陆的过程中，这个方法根据用户名取查找用户
    //如果用户不存在，抛出异常，否则返回Hr
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Hr hr = hrMapper.loadUserByUsername(username);
        if(hr == null){
            throw new UsernameNotFoundException("用户名不存在");
        }
        hr.setRoles(hrMapper.getHrRolesById(hr.getId()));
        return hr;

    }

    /**
     * 查找所有Hr
     * @param keywords
     * @return List
     */
    public List<Hr> getAllHrs(String keywords) {
        return hrMapper.getAllHrs(HrUtils.getCurrentHr().getId(),keywords);
    }

    /**
     * 更新hr
     * @param hr
     * @return hr.id
     */
    public Integer updateHr(Hr hr) {
        return hrMapper.updateByPrimaryKeySelective(hr);
    }

    /**
     * 更新hr的角色
     * @param hrid
     * @param rids
     * @return boolean
     */
    @Transactional
    public boolean updateHrRole(Integer hrid, Integer[] rids) {
        //先删除hr的角色
        hrRoleMapper.deleteByHrid(hrid);
        //再给hr添加角色
        return hrRoleMapper.addRole(hrid,rids)==rids.length;

    }

    /**
     * 删除hr
     * @param id
     * @return int
     */
    public Integer deleteHrById(Integer id) {
        return hrMapper.deleteByPrimaryKey(id);
    }
}
