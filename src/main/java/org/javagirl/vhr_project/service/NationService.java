package org.javagirl.vhr_project.service;

import org.javagirl.vhr_project.mapper.NationMapper;
import org.javagirl.vhr_project.model.Nation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NationService {
    @Autowired
    NationMapper nationMapper;
    public List<Nation> getAllNations() {
        return nationMapper.getAllNations();
    }
}
