package org.javagirl.vhr_project.converter;


import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 当前端传来一个日期时，需要服务端自定义参数绑定，将前端的日期进行转换
 */
@Component
public class DateConverter implements Converter<String, Date> {
//    接收前端传来的日期字符串，通过SimpleDateFormat将字符串转为一个Date对象返回
    SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public Date convert(String s) {
        try {
            return sdf.parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
