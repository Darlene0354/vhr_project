package org.javagirl.vhr_project;


import org.javagirl.vhr_project.controller.emp.EmpBasicController;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
@RunWith(SpringRunner.class)
@SpringBootTest
public class VhrProjectApplicationTests {

    public MockMvc mockMvc;
    @Autowired
    WebApplicationContext webApplicationContext;
    @BeforeEach
    public void setUp(){
//        mockMvc= MockMvcBuilders.webAppContextSetup((WebApplicationContext) new EmpBasicController()).build();
        mockMvc= MockMvcBuilders.standaloneSetup( new EmpBasicController()).build();
    }

    @Test
    public void contextLoads() throws Exception {
//        mockMvc.perform(MockMvcRequestBuilders.get("/").accept(MediaType.APPLICATION_JSON_UTF8)).andExpect(MockMvcResultMatchers.status().isOk());
        mockMvc.perform(MockMvcRequestBuilders.get("/").accept(MediaType.APPLICATION_JSON_UTF8));
    }

}
